cmake_minimum_required(VERSION 3.14)

include(cmake/prelude.cmake)

project(
    strong_typedef
    VERSION 0.1.0
    DESCRIPTION "A class template that creates a new type that is distinct from
the underlying type, but convertible to and from it"
    HOMEPAGE_URL "https://github.com/anthonywilliams/strong_typedef"
    LANGUAGES CXX
)

include(cmake/project-is-top-level.cmake)
include(cmake/variables.cmake)

# ---- Declare library ----

add_library(strong_typedef INTERFACE)
add_library(jss ALIAS strong_typedef)

set_property(
    TARGET strong_typedef PROPERTY
    EXPORT_NAME jss
)

target_include_directories(
    strong_typedef ${warning_guard}
    INTERFACE
    "$<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>"
)

target_compile_features(strong_typedef INTERFACE cxx_std_17)

# ---- Developer mode ----

if(NOT strong_typedef_DEVELOPER_MODE)
  return()
elseif(NOT PROJECT_IS_TOP_LEVEL)
  message(
      AUTHOR_WARNING
      "Developer mode is intended for developers of strong_typedef"
  )
endif()

include(cmake/dev-mode.cmake)
