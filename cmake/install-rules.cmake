if(PROJECT_IS_TOP_LEVEL)
  set(CMAKE_INSTALL_INCLUDEDIR include/strong_typedef CACHE PATH "")
endif()

# Project is configured with no languages, so tell GNUInstallDirs the lib dir
set(CMAKE_INSTALL_LIBDIR lib CACHE PATH "")

include(CMakePackageConfigHelpers)
include(GNUInstallDirs)

# find_package(<package>) call for consumers to find this project
set(package strong_typedef)

install(
    DIRECTORY include/
    DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}"
    COMPONENT strong_typedef_Development
)

install(
    TARGETS strong_typedef
    EXPORT strong_typedefTargets
    INCLUDES DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}"
)

write_basic_package_version_file(
    "${package}ConfigVersion.cmake"
    COMPATIBILITY SameMajorVersion
    ARCH_INDEPENDENT
)

# Allow package maintainers to freely override the path for the configs
set(
    strong_typedef_INSTALL_CMAKEDIR "${CMAKE_INSTALL_DATADIR}/${package}"
    CACHE PATH "CMake package config location relative to the install prefix"
)
mark_as_advanced(strong_typedef_INSTALL_CMAKEDIR)

install(
    FILES cmake/install-config.cmake
    DESTINATION "${strong_typedef_INSTALL_CMAKEDIR}"
    RENAME "${package}Config.cmake"
    COMPONENT strong_typedef_Development
)

install(
    FILES "${PROJECT_BINARY_DIR}/${package}ConfigVersion.cmake"
    DESTINATION "${strong_typedef_INSTALL_CMAKEDIR}"
    COMPONENT strong_typedef_Development
)

install(
    EXPORT strong_typedefTargets
    NAMESPACE jss::
    DESTINATION "${strong_typedef_INSTALL_CMAKEDIR}"
    COMPONENT strong_typedef_Development
)

if(PROJECT_IS_TOP_LEVEL)
  include(CPack)
endif()
