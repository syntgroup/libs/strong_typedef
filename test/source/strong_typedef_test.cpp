#include <iostream>
#include <sstream>
#include <type_traits>

#include "strong_typedef/strong_typedef.hpp"

#include <assert.h>
#include <gtest/gtest.h>

TEST(test_strong_typedef, is_not_original)
{
  using myType  = jss::strong_typedef<struct MyTag, int>;
  auto result_1 = !std::is_same<int, myType>::value;
  ASSERT_TRUE(result_1) << "Strong typedef should not be the same";

  using myType2 = jss::strong_typedef<struct MyTag2, int>;
  auto result_2 = !std::is_same<myType, myType2>::value;
  ASSERT_TRUE(result_2)
    << "Strong typedefs with different tags should not be the same";
}

TEST(test_strong_typedef, explicitly_convertible_from_source)
{
  jss::strong_typedef<struct Tag, int> st(42);
  ASSERT_EQ(st.underlying_value(), 42);
}

using ConvTestTypedef = jss::strong_typedef<struct ConvTest, int>;

using small_result = char;
struct large_result
{
  small_result dummy[2];
};

small_result conv_test(ConvTestTypedef);
large_result
conv_test(...);

TEST(test_strong_typedef, not_implicitly_convertible_from_source)
{
  ASSERT_EQ(sizeof(conv_test(42)), sizeof(large_result));
}

TEST(test_strong_typedef, explicitly_convertible_to_source)
{
  jss::strong_typedef<struct Tag, int> st(42);
  EXPECT_EQ(static_cast<int>(st), 42);
}

small_result
conv_test_to_int(int);
large_result
conv_test_to_int(...);

TEST(test_strong_typedef, not_implicitly_convertible_to_source)
{
  jss::strong_typedef<struct Tag, int> st(42);
  ASSERT_EQ(sizeof(conv_test_to_int(st)), sizeof(large_result));
}

TEST(test_strong_typedef, is_copyable_and_movable)
{
  std::string const s =
    "hello there this is quote a long string abcdefghijklmnopoq";
  using ST = jss::strong_typedef<struct Tag, std::string>;
  ST st(s);

  ASSERT_TRUE(std::is_copy_constructible<ST>::value)
    << "strong typedef should be copy constructible";
  ASSERT_TRUE(std::is_copy_assignable<ST>::value)
    << "strong typedef should be copy assignable";
  ASSERT_TRUE(std::is_move_constructible<ST>::value)
    << "strong typedef should be move constructible";
  ASSERT_TRUE(std::is_move_assignable<ST>::value)
    << "strong typedef should be move assignable";

  ST st2(st);

  EXPECT_EQ(static_cast<std::string>(st), s);
  EXPECT_EQ(static_cast<std::string>(st2), s);

  ST st3(std::move(st));
  EXPECT_EQ(static_cast<std::string>(st), "");
  EXPECT_EQ(static_cast<std::string>(st2), s);
  EXPECT_EQ(static_cast<std::string>(st3), s);

  st = std::move(st2);
  EXPECT_EQ(static_cast<std::string>(st), s);
  EXPECT_EQ(static_cast<std::string>(st2), "");
  EXPECT_EQ(static_cast<std::string>(st3), s);

  st2 = st;
  EXPECT_EQ(static_cast<std::string>(st), s);
  EXPECT_EQ(static_cast<std::string>(st2), s);
  EXPECT_EQ(static_cast<std::string>(st3), s);
}

template<typename T>
typename std::enable_if<sizeof(std::declval<T const&>() ==
                               std::declval<T const&>()) != 0,
                        small_result>::type
test_equality(int);
template<typename T>
large_result
test_equality(...);

template<typename T>
typename std::enable_if<sizeof(std::declval<T const&>() !=
                               std::declval<T const&>()) != 0,
                        small_result>::type
test_inequality(int);
template<typename T>
large_result
test_inequality(...);

TEST(test_strong_typedef, by_default_is_not_equality_comparable)
{
  using ST = jss::strong_typedef<struct Tag, std::string>;

  ASSERT_EQ(sizeof(test_equality<ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_equality<std::string>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_inequality<ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_inequality<std::string>(0)), sizeof(small_result));
}

TEST(test_strong_typedef, can_get_underlying_value_and_type)
{
  struct LocalTag;
  struct X
  {
    int i;
  };

  using ST = jss::strong_typedef<LocalTag, X>;

  ST st({ 42 });

  auto result_1{ std::is_same<typename ST::underlying_value_type, X>::value };
  ASSERT_TRUE(result_1) << "Strong typedef must expose underlying type";

  auto result_2{
    std::is_same<decltype(std::declval<ST&>().underlying_value()), X&>::value
  };
  ASSERT_TRUE(result_2) << "Strong typedef must expose underlying value";

  auto result_3{
    std::is_same<decltype(std::declval<ST const&>().underlying_value()),
                 X const&>::value
  };
  ASSERT_TRUE(result_3) << "Strong typedef must expose underlying value";

  EXPECT_EQ(st.underlying_value().i, 42);
}

TEST(test_strong_typedef, is_equality_comparable_if_tagged_as_such)
{
  using ST =
    jss::strong_typedef<struct Tag,
                        std::string,
                        jss::strong_typedef_properties::equality_comparable>;

  ASSERT_EQ(sizeof(test_equality<ST>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_equality<std::string>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_inequality<ST>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_inequality<std::string>(0)), sizeof(small_result));
}

template<typename T>
typename std::enable_if<sizeof(std::declval<T&>()++) != 0, small_result>::type
test_post_incrementable(int);
template<typename T>
large_result
test_post_incrementable(...);

template<typename T>
typename std::enable_if<sizeof(++std::declval<T&>()) != 0, small_result>::type
test_pre_incrementable(int);
template<typename T>
large_result
test_pre_incrementable(...);

TEST(test_strong_typedef, by_default_is_not_incrementable)
{
  using ST = jss::strong_typedef<struct Tag, int>;

  ASSERT_EQ(sizeof(test_post_incrementable<ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_post_incrementable<std::string>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_post_incrementable<int>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_pre_incrementable<ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_pre_incrementable<std::string>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_pre_incrementable<int>(0)), sizeof(small_result));
}

TEST(test_strong_typedef, is_incrementable_if_tagged_as_such)
{
  using ST_post =
    jss::strong_typedef<struct Tag,
                        int,
                        jss::strong_typedef_properties::post_incrementable>;
  using ST_pre =
    jss::strong_typedef<struct Tag,
                        int,
                        jss::strong_typedef_properties::pre_incrementable>;
  using ST_both =
    jss::strong_typedef<struct ST_both_tag,
                        int,
                        jss::strong_typedef_properties::incrementable>;

  ASSERT_EQ(sizeof(test_pre_incrementable<ST_pre>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_pre_incrementable<ST_post>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_post_incrementable<ST_pre>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_post_incrementable<ST_post>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_pre_incrementable<ST_both>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_post_incrementable<ST_both>(0)), sizeof(small_result));

  ST_post post1(42);
  ST_post post2 = post1++;

  EXPECT_EQ(post1.underlying_value(), 43);
  EXPECT_EQ(post2.underlying_value(), 42);

  ST_pre pre1(42);
  ST_pre& pre2 = ++pre1;

  EXPECT_EQ(&pre2, &pre1);

  EXPECT_EQ(pre1.underlying_value(), 43);
}

template<typename T>
typename std::enable_if<sizeof(std::declval<T&>()--) != 0, small_result>::type
test_post_decrementable(int);
template<typename T>
large_result
test_post_decrementable(...);

template<typename T>
typename std::enable_if<sizeof(--std::declval<T&>()) != 0, small_result>::type
test_pre_decrementable(int);
template<typename T>
large_result
test_pre_decrementable(...);

TEST(test_strong_typedef, by_default_is_not_decrementable)
{
  using ST = jss::strong_typedef<struct Tag, int>;

  ASSERT_EQ(sizeof(test_post_decrementable<ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_post_decrementable<std::string>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_post_decrementable<int>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_pre_decrementable<ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_pre_decrementable<std::string>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_pre_decrementable<int>(0)), sizeof(small_result));
}

TEST(test_strong_typedef, is_decrementable_if_tagged_as_such)
{
  using ST_post =
    jss::strong_typedef<struct Tag,
                        int,
                        jss::strong_typedef_properties::post_decrementable>;
  using ST_pre =
    jss::strong_typedef<struct Tag,
                        int,
                        jss::strong_typedef_properties::pre_decrementable>;

  ASSERT_EQ(sizeof(test_pre_decrementable<ST_pre>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_pre_decrementable<ST_post>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_post_decrementable<ST_pre>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_post_decrementable<ST_post>(0)), sizeof(small_result));

  ST_post post1(42);
  ST_post post2 = post1--;

  EXPECT_EQ(post1.underlying_value(), 41);
  EXPECT_EQ(post2.underlying_value(), 42);

  ST_pre pre1(42);
  ST_pre& pre2 = --pre1;

  EXPECT_EQ(&pre2, &pre1);

  EXPECT_EQ(pre1.underlying_value(), 41);
}

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() + std::declval<U&>()) != 0,
                        small_result>::type
test_addable(int);
template<typename T, typename U>
large_result
test_addable(...);

TEST(test_strong_typedef, by_default_is_not_addable)
{
  using ST = jss::strong_typedef<struct Tag, std::string>;

  ASSERT_EQ(sizeof(test_addable<ST, ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_addable<ST, std::string>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_addable<ST, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_addable<std::string, ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_addable<int, ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_addable<std::string, std::string>(0)),
            sizeof(small_result));
  ASSERT_EQ(sizeof(test_addable<int, int>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_addable<std::string, int>(0)), sizeof(large_result));
}

TEST(test_strong_typedef, is_addable_if_tagged_as_such)
{
  using ST = jss::strong_typedef<struct Tag,
                                 std::string,
                                 jss::strong_typedef_properties::addable>;
  ASSERT_EQ(sizeof(test_addable<ST, ST>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_addable<ST, std::string>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_addable<ST, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_addable<std::string, ST>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_addable<int, ST>(0)), sizeof(large_result));

  ST st1("hello");
  ST st2 = st1 + " world";
  EXPECT_EQ(st1.underlying_value(), "hello");
  EXPECT_EQ(st2.underlying_value(), "hello world");
  auto st3 = "goodbye" + st1;
  EXPECT_EQ(st1.underlying_value(), "hello");
  EXPECT_EQ(st3.underlying_value(), "goodbyehello");
  auto st4 = st1 + st1;
  EXPECT_EQ(st1.underlying_value(), "hello");
  EXPECT_EQ(st4.underlying_value(), "hellohello");
}

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() - std::declval<U&>()) != 0,
                        small_result>::type
test_subtractable(int);
template<typename T, typename U>
large_result
test_subtractable(...);

TEST(test_strong_typedef, by_default_is_not_subtractable)
{
  using ST = jss::strong_typedef<struct Tag, int>;

  ASSERT_EQ(sizeof(test_subtractable<ST, ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_subtractable<ST, std::string>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_subtractable<ST, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_subtractable<std::string, ST>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_subtractable<int, ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_subtractable<std::string, std::string>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_subtractable<int, int>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_subtractable<std::string, int>(0)),
            sizeof(large_result));
}

TEST(test_strong_typedef, is_subtractable_if_tagged_as_such)
{
  using ST = jss::strong_typedef<struct Tag,
                                 int,
                                 jss::strong_typedef_properties::subtractable>;
  ASSERT_EQ(sizeof(test_subtractable<ST, ST>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_subtractable<ST, std::string>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_subtractable<ST, int>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_subtractable<std::string, ST>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_subtractable<int, ST>(0)), sizeof(small_result));
}

template<typename T, typename U>
typename std::enable_if<
  std::is_same<bool,
               decltype(std::declval<T const&>() <
                        std::declval<U const&>())>::value &&
    std::is_same<bool,
                 decltype(std::declval<T const&>() >
                          std::declval<U const&>())>::value &&
    std::is_same<bool,
                 decltype(std::declval<T const&>() <=
                          std::declval<U const&>())>::value &&
    std::is_same<bool,
                 decltype(std::declval<T const&>() >=
                          std::declval<U const&>())>::value,
  small_result>::type
test_ordered(int);
template<typename T, typename U>
large_result
test_ordered(...);

TEST(test_strong_typedef, by_default_is_not_ordered)
{
  using ST = jss::strong_typedef<struct Tag, int>;

  ASSERT_EQ(sizeof(test_ordered<ST, ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_ordered<ST, std::string>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_ordered<ST, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_ordered<std::string, ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_ordered<int, ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_ordered<std::string, std::string>(0)),
            sizeof(small_result));
  ASSERT_EQ(sizeof(test_ordered<int, int>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_ordered<float, int>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_ordered<std::string, int>(0)), sizeof(large_result));
}

TEST(test_strong_typedef, is_ordered_if_tagged_as_such)
{
  using ST = jss::
    strong_typedef<struct Tag, int, jss::strong_typedef_properties::ordered>;
  using ST2 = jss::
    strong_typedef<struct Tag2, int, jss::strong_typedef_properties::ordered>;
  ASSERT_EQ(sizeof(test_ordered<ST, ST>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_ordered<ST, ST2>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_ordered<ST, std::string>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_ordered<ST, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_ordered<std::string, ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_ordered<int, ST>(0)), sizeof(large_result));

  ST const st1(42);
  ST const st2(43);

  EXPECT_TRUE(!(st1 < st1));
  EXPECT_TRUE(!(st1 > st1));
  EXPECT_TRUE(st1 <= st1);
  EXPECT_TRUE(st1 >= st1);
  EXPECT_TRUE(st1 < st2);
  EXPECT_TRUE(st1 <= st2);
  EXPECT_TRUE(!(st2 < st1));
  EXPECT_TRUE(!(st2 <= st1));
  EXPECT_TRUE(st2 > st1);
  EXPECT_TRUE(st2 >= st1);
  EXPECT_TRUE(!(st1 > st2));
  EXPECT_TRUE(!(st1 >= st2));
}

TEST(test_strong_typedef, is_mixed_ordered_if_tagged_as_such)
{
  using ST =
    jss::strong_typedef<struct Tag,
                        int,
                        jss::strong_typedef_properties::mixed_ordered<int>>;
  using ST2 =
    jss::strong_typedef<struct Tag2,
                        int,
                        jss::strong_typedef_properties::mixed_ordered<int>>;

  ASSERT_EQ(sizeof(test_ordered<ST, ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_ordered<ST, ST2>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_ordered<ST, std::string>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_ordered<ST, int>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_ordered<std::string, ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_ordered<int, ST>(0)), sizeof(small_result));

  ST constexpr st1(42);
  int const st2(43);

  EXPECT_TRUE(st1 < st2);
  EXPECT_TRUE(st1 <= st2);
  EXPECT_TRUE(!(st2 < st1));
  EXPECT_TRUE(!(st2 <= st1));
  EXPECT_TRUE(st2 > st1);
  EXPECT_TRUE(st2 >= st1);
  EXPECT_TRUE(!(st1 > st2));
  EXPECT_TRUE(!(st1 >= st2));
}

template<typename T>
typename std::enable_if<
  std::is_convertible<decltype(std::hash<T>()(std::declval<T const&>())),
                      size_t>::value,
  small_result>::type
test_hashable(int);
template<typename T>
large_result
test_hashable(...);

TEST(test_strong_typedef, by_default_is_not_hashable)
{
  using ST = jss::strong_typedef<struct Tag, int>;

  ASSERT_EQ(sizeof(test_hashable<ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_hashable<std::string>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_hashable<int>(0)), sizeof(small_result));
}

TEST(test_strong_typedef, is_hashable_if_tagged_as_such)
{
  using ST = jss::strong_typedef<struct Tag,
                                 std::string,
                                 jss::strong_typedef_properties::hashable>;
  ASSERT_EQ(sizeof(test_hashable<ST>(0)), sizeof(small_result));

  std::string s("hello");
  ST st(s);
  EXPECT_EQ(std::hash<ST>()(st), std::hash<std::string>()(s));
}

template<typename T>
typename std::enable_if<
  std::is_convertible<decltype(std::declval<std::ostream&>()
                               << std::declval<T const&>()),
                      std::ostream&>::value,
  small_result>::type
test_streamable(int);
template<typename T>
large_result
test_streamable(...);

TEST(test_strong_typedef, by_default_is_not_streamable)
{
  using ST = jss::strong_typedef<struct Tag, int>;

  ASSERT_EQ(sizeof(test_streamable<ST>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_streamable<std::string>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_streamable<int>(0)), sizeof(small_result));
}

TEST(test_strong_typedef, is_streamable_if_tagged_as_such)
{
  using ST = jss::strong_typedef<struct Tag,
                                 std::string,
                                 jss::strong_typedef_properties::streamable>;

  ASSERT_EQ(sizeof(test_streamable<ST>(0)), sizeof(small_result))
    << "Must be streamable when tagged";

  std::string s("hello");
  ST st(s);
  std::stringstream os;
  os << st;

  EXPECT_EQ(os.str(), s);
}

TEST(test_strong_typedef, properties_can_be_combined)
{
  using ST = jss::strong_typedef<struct Tag,
                                 std::string,
                                 jss::strong_typedef_properties::streamable,
                                 jss::strong_typedef_properties::hashable,
                                 jss::strong_typedef_properties::comparable>;

  ASSERT_EQ(sizeof(test_streamable<ST>(0)), sizeof(small_result))
    << "Must be streamable when tagged";
  ASSERT_EQ(sizeof(test_hashable<ST>(0)), sizeof(small_result))
    << "Must be hashable when tagged";
  ASSERT_EQ(sizeof(test_ordered<ST, ST>(0)), sizeof(small_result))
    << "Must be ordered when tagged";
  ASSERT_EQ(sizeof(test_equality<ST>(0)), sizeof(small_result))
    << "Must be equality-comparable when tagged";
}

TEST(test_strong_typedef, is_default_constructible)
{
  struct X
  {
    int value;
    X()
      : value(42)
    {}
  };

  using ST = jss::strong_typedef<struct sometag, X>;

  ST st;

  EXPECT_EQ(static_cast<X>(st).value, 42);

  jss::strong_typedef<struct footag, int> i;

  EXPECT_EQ(static_cast<int>(i), 0);
}

TEST(test_strong_typedef, can_support_difference_with_other_type)
{
  using difference_type = jss::strong_typedef<struct difftag, int>;
  using ST              = jss::strong_typedef<
    struct sometag,
    int,
    jss::strong_typedef_properties::difference<difference_type>>;

  ASSERT_EQ(sizeof(test_subtractable<ST, ST>(0)), sizeof(small_result));

  auto result{ std::is_same<decltype(std::declval<ST const&>() -
                                     std::declval<ST const&>()),
                            difference_type>::value };
  ASSERT_TRUE(result);

  ST st1(45);
  ST st2(99);
  difference_type res = st2 - st1;
  EXPECT_EQ(res.underlying_value(), 54);
}

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() * std::declval<U&>()) != 0,
                        small_result>::type
test_multiplicable(int);
template<typename T, typename U>
large_result
test_multiplicable(...);

TEST(test_strong_typedef, self_multiplication)
{
  ASSERT_EQ(sizeof(test_multiplicable<int, int>(0)), sizeof(small_result));

  using ST1 = jss::strong_typedef<struct Tag1, int>;
  ASSERT_EQ(sizeof(test_multiplicable<ST1, ST1>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_multiplicable<ST1, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_multiplicable<int, ST1>(0)), sizeof(large_result));
}

TEST(test_strong_typedef, mixed_multiplication)
{
  using ST2 =
    jss::strong_typedef<struct Tag2,
                        int,
                        jss::strong_typedef_properties::self_multiplicable>;
  ASSERT_EQ(sizeof(test_multiplicable<ST2, ST2>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_multiplicable<ST2, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_multiplicable<int, ST2>(0)), sizeof(large_result));

  ST2 a(5);
  ST2 b(6);
  ST2 c = a * b;
  EXPECT_EQ(c.underlying_value(), 30);

  using ST3 = jss::strong_typedef<
    struct Tag3,
    int,
    jss::strong_typedef_properties::mixed_multiplicable<int>>;
  ASSERT_EQ(sizeof(test_multiplicable<ST3, ST3>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_multiplicable<ST3, int>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_multiplicable<int, ST3>(0)), sizeof(small_result));

  ST3 d(9);
  int e(7);
  ST3 f = d * e;
  EXPECT_EQ(f.underlying_value(), 63);

  using ST4 =
    jss::strong_typedef<struct Tag4,
                        int,
                        jss::strong_typedef_properties::multiplicable>;
  ASSERT_EQ(sizeof(test_multiplicable<ST4, ST4>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_multiplicable<ST4, int>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_multiplicable<int, ST4>(0)), sizeof(small_result));
}

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() / std::declval<U&>()) != 0,
                        small_result>::type
test_divisible(int);
template<typename T, typename U>
large_result
test_divisible(...);

TEST(test_strong_typedef, self_division)
{
  ASSERT_EQ(sizeof(test_divisible<int, int>(0)), sizeof(small_result));

  using ST1 = jss::strong_typedef<struct Tag1, int>;
  ASSERT_EQ(sizeof(test_divisible<ST1, ST1>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_divisible<ST1, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_divisible<int, ST1>(0)), sizeof(large_result));
}

TEST(test_strong_typedef, mixed_division)
{
  using ST2 =
    jss::strong_typedef<struct Tag2,
                        int,
                        jss::strong_typedef_properties::self_divisible>;
  ASSERT_EQ(sizeof(test_divisible<ST2, ST2>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_divisible<ST2, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_divisible<int, ST2>(0)), sizeof(large_result));

  ST2 a(42);
  ST2 b(6);
  ST2 c = a / b;
  EXPECT_EQ(c.underlying_value(), 7);

  using ST3 =
    jss::strong_typedef<struct Tag3,
                        int,
                        jss::strong_typedef_properties::mixed_divisible<int>>;
  ASSERT_EQ(sizeof(test_divisible<ST3, ST3>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_divisible<ST3, int>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_divisible<int, ST3>(0)), sizeof(small_result));

  ST3 d(99);
  int e(11);
  ST3 f = d / e;
  EXPECT_EQ(f.underlying_value(), 9);

  using ST4 = jss::
    strong_typedef<struct Tag4, int, jss::strong_typedef_properties::divisible>;
  ASSERT_EQ(sizeof(test_divisible<ST4, ST4>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_divisible<ST4, int>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_divisible<int, ST4>(0)), sizeof(small_result));
}

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() % std::declval<U&>()) != 0,
                        small_result>::type
test_modulus(int);
template<typename T, typename U>
large_result
test_modulus(...);

TEST(test_strong_typedef, self_modulus)
{
  ASSERT_EQ(sizeof(test_modulus<int, int>(0)), sizeof(small_result));

  using ST1 = jss::strong_typedef<struct Tag1, int>;
  ASSERT_EQ(sizeof(test_modulus<ST1, ST1>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_modulus<ST1, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_modulus<int, ST1>(0)), sizeof(large_result));
}

TEST(test_strong_typedef, mixed_modulus)
{
  std::cout << __FUNCTION__ << std::endl;

  using ST2 = jss::strong_typedef<struct Tag2,
                                  int,
                                  jss::strong_typedef_properties::self_modulus>;
  ASSERT_EQ(sizeof(test_modulus<ST2, ST2>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_modulus<ST2, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_modulus<int, ST2>(0)), sizeof(large_result));

  constexpr ST2 a(42);
  constexpr ST2 b(5);
  constexpr ST2 c = a % b;
  ASSERT_EQ(c.underlying_value(), 2);

  using ST3 =
    jss::strong_typedef<struct Tag3,
                        int,
                        jss::strong_typedef_properties::mixed_modulus<int>>;
  ASSERT_EQ(sizeof(test_modulus<ST3, ST3>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_modulus<ST3, int>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_modulus<int, ST3>(0)), sizeof(small_result));

  constexpr ST3 d(99);
  constexpr int e(8);
  constexpr ST3 f = d % e;
  ASSERT_EQ(f.underlying_value(), 3);

  using ST4 = jss::
    strong_typedef<struct Tag4, int, jss::strong_typedef_properties::modulus>;
  ASSERT_EQ(sizeof(test_modulus<ST4, ST4>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_modulus<ST4, int>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_modulus<int, ST4>(0)), sizeof(small_result));
}

TEST(test_strong_typedef, ratio)
{
  using ratio_type = jss::strong_typedef<struct difftag, int>;
  using ST =
    jss::strong_typedef<struct sometag,
                        int,
                        jss::strong_typedef_properties::ratio<ratio_type>>;

  ASSERT_EQ(sizeof(test_divisible<ST, ST>(0)), sizeof(small_result));

  auto result{ std::is_same<decltype(std::declval<ST const&>() /
                                     std::declval<ST const&>()),
                            ratio_type>::value };
  ASSERT_TRUE(result);

  ST st1(125);
  ST st2(5);

  ratio_type res = st1 / st2;
  EXPECT_EQ(res.underlying_value(), 25);
}

TEST(test_strong_typedef, constexpr_comparison)
{
  using ST = jss::
    strong_typedef<struct Tag, int, jss::strong_typedef_properties::comparable>;

  constexpr ST st1(42);
  constexpr ST st2(43);
  constexpr bool r1 = st1 == st2;
  constexpr bool r2 = st1 < st2;
  constexpr bool r3 = st1 > st2;
  constexpr bool r4 = st1 <= st2;
  constexpr bool r5 = st1 >= st2;
  constexpr bool r6 = st1 != st2;
  ASSERT_TRUE(!r1);
  ASSERT_TRUE(r2);
  ASSERT_TRUE(!r3);
  ASSERT_TRUE(r4);
  ASSERT_TRUE(!r5);
  ASSERT_TRUE(r6);
}

TEST(test_strong_typedef, constexpr_addition)
{
  using ST = jss::
    strong_typedef<struct Tag, int, jss::strong_typedef_properties::addable>;

  constexpr ST st1(42);
  constexpr ST st2(3);

  constexpr ST st3 = st1 + st2;
  constexpr ST st4 = st1 + 1;
  constexpr ST st5 = -1 + st1;

  ASSERT_EQ(st3.underlying_value(), 45);
  ASSERT_EQ(st4.underlying_value(), 43);
  ASSERT_EQ(st5.underlying_value(), 41);
}

TEST(test_strong_typedef, constexpr_subtraction)
{
  using ST = jss::strong_typedef<struct Tag,
                                 int,
                                 jss::strong_typedef_properties::subtractable>;

  constexpr ST st1(42);
  constexpr ST st2(3);

  constexpr ST st3 = st1 - st2;
  constexpr ST st4 = st1 - 1;
  constexpr ST st5 = -1 - st1;

  ASSERT_EQ(st3.underlying_value(), 39);
  ASSERT_EQ(st4.underlying_value(), 41);
  ASSERT_EQ(st5.underlying_value(), -43);
}

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() | std::declval<U&>()) != 0,
                        small_result>::type
test_bitwise_or(int);
template<typename T, typename U>
large_result
test_bitwise_or(...);

TEST(test_strong_typedef, bitwise_or)
{
  using ST_plain = jss::strong_typedef<struct Plain, int>;
  using ST_self =
    jss::strong_typedef<struct Self,
                        int,
                        jss::strong_typedef_properties::self_bitwise_or>;
  using ST_mixed =
    jss::strong_typedef<struct Mixed,
                        int,
                        jss::strong_typedef_properties::mixed_bitwise_or<int>>;

  ASSERT_EQ(sizeof(test_bitwise_or<ST_plain, ST_plain>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_or<ST_plain, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_or<int, ST_plain>(0)), sizeof(large_result));

  ASSERT_EQ(sizeof(test_bitwise_or<ST_self, ST_self>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_bitwise_or<ST_self, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_or<int, ST_self>(0)), sizeof(large_result));

  ASSERT_EQ(sizeof(test_bitwise_or<ST_mixed, ST_mixed>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_or<ST_mixed, int>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_bitwise_or<int, ST_mixed>(0)), sizeof(small_result));

  constexpr ST_self st1{ 0x1842 };
  constexpr ST_self st2(0x8214);

  constexpr ST_self st3 = st1 | st2;

  ASSERT_EQ(st3.underlying_value(), 0x9a56);

  constexpr ST_mixed st4{ 0x1842a5 };
  constexpr int i1(0x82145a);

  constexpr ST_mixed st5 = st4 | i1;
  constexpr ST_mixed st6 = i1 | st4;

  ASSERT_EQ(st5.underlying_value(), 0x9a56ff);
  ASSERT_EQ(st6.underlying_value(), 0x9a56ff);
}

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() & std::declval<U&>()) != 0,
                        small_result>::type
test_bitwise_and(int);
template<typename T, typename U>
large_result
test_bitwise_and(...);

TEST(test_strong_typedef, bitwise_and)
{
  using ST_plain = jss::strong_typedef<struct Plain, int>;
  using ST_self =
    jss::strong_typedef<struct Self,
                        int,
                        jss::strong_typedef_properties::self_bitwise_and>;
  using ST_mixed =
    jss::strong_typedef<struct Mixed,
                        int,
                        jss::strong_typedef_properties::mixed_bitwise_and<int>>;

  ASSERT_EQ(sizeof(test_bitwise_and<ST_plain, ST_plain>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_and<ST_plain, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_and<int, ST_plain>(0)), sizeof(large_result));

  ASSERT_EQ(sizeof(test_bitwise_and<ST_self, ST_self>(0)),
            sizeof(small_result));
  ASSERT_EQ(sizeof(test_bitwise_and<ST_self, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_and<int, ST_self>(0)), sizeof(large_result));

  ASSERT_EQ(sizeof(test_bitwise_and<ST_mixed, ST_mixed>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_and<ST_mixed, int>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_bitwise_and<int, ST_mixed>(0)), sizeof(small_result));

  constexpr ST_self st1{ 0x1876 };
  constexpr ST_self st2(0x9af4);

  constexpr ST_self st3 = st1 & st2;

  ASSERT_EQ(st3.underlying_value(), 0x1874);

  constexpr ST_mixed st4{ 0xf3c5a5 };
  constexpr int i1(0x83945a);

  constexpr ST_mixed st5 = st4 & i1;
  constexpr ST_mixed st6 = i1 & st4;

  ASSERT_EQ(st5.underlying_value(), 0x838400);
  ASSERT_EQ(st6.underlying_value(), 0x838400);
}
template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() ^ std::declval<U&>()) != 0,
                        small_result>::type
test_bitwise_xor(int);
template<typename T, typename U>
large_result
test_bitwise_xor(...);

TEST(test_strong_typedef, bitwise_xor)
{
  using ST_plain = jss::strong_typedef<struct Plain, int>;
  using ST_self =
    jss::strong_typedef<struct Self,
                        int,
                        jss::strong_typedef_properties::self_bitwise_xor>;
  using ST_mixed =
    jss::strong_typedef<struct Mixed,
                        int,
                        jss::strong_typedef_properties::mixed_bitwise_xor<int>>;

  ASSERT_EQ(sizeof(test_bitwise_xor<ST_plain, ST_plain>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_xor<ST_plain, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_xor<int, ST_plain>(0)), sizeof(large_result));

  ASSERT_EQ(sizeof(test_bitwise_xor<ST_self, ST_self>(0)),
            sizeof(small_result));
  ASSERT_EQ(sizeof(test_bitwise_xor<ST_self, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_xor<int, ST_self>(0)), sizeof(large_result));

  ASSERT_EQ(sizeof(test_bitwise_xor<ST_mixed, ST_mixed>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_xor<ST_mixed, int>(0)), sizeof(small_result));
  ASSERT_EQ(sizeof(test_bitwise_xor<int, ST_mixed>(0)), sizeof(small_result));

  constexpr ST_self st1{ 0x1876 };
  constexpr ST_self st2(0x9af4);

  constexpr ST_self st3 = st1 ^ st2;

  ASSERT_EQ(st3.underlying_value(), 0x8282);

  constexpr ST_mixed st4{ 0xf3c5a5 };
  constexpr int i1(0x83945a);

  constexpr ST_mixed st5 = st4 ^ i1;
  constexpr ST_mixed st6 = i1 ^ st4;

  ASSERT_EQ(st5.underlying_value(), 0x7051ff);
  ASSERT_EQ(st6.underlying_value(), 0x7051ff);
}

template<typename T>
typename std::enable_if<sizeof(~std::declval<T&>()) != 0, small_result>::type
test_bitwise_not(int);
template<typename T>
large_result
test_bitwise_not(...);

TEST(test_strong_typedef, bitwise_not)
{
  using ST_plain = jss::strong_typedef<struct Plain, int>;
  using ST_self =
    jss::strong_typedef<struct Self,
                        int,
                        jss::strong_typedef_properties::bitwise_not>;

  ASSERT_EQ(sizeof(test_bitwise_not<ST_plain>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_not<ST_self>(0)), sizeof(small_result));

  constexpr ST_self st1{ 0x1876 };
  constexpr ST_self st2 = ~st1;

  ASSERT_EQ(st2.underlying_value(), 0xffffe789);
}

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() << std::declval<U&>()) != 0,
                        small_result>::type
test_bitwise_left_shift(int);
template<typename T, typename U>
large_result
test_bitwise_left_shift(...);

TEST(test_strong_typedef, bitwise_left_shift)
{
  using ST_plain = jss::strong_typedef<struct Plain, int>;
  using ST_mixed = jss::strong_typedef<
    struct Mixed,
    int,
    jss::strong_typedef_properties::bitwise_left_shift<int>>;

  ASSERT_EQ(sizeof(test_bitwise_left_shift<ST_plain, ST_plain>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_left_shift<ST_plain, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_left_shift<int, ST_plain>(0)),
            sizeof(large_result));

  ASSERT_EQ(sizeof(test_bitwise_left_shift<ST_mixed, ST_mixed>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_left_shift<ST_mixed, int>(0)),
            sizeof(small_result));
  ASSERT_EQ(sizeof(test_bitwise_left_shift<int, ST_mixed>(0)),
            sizeof(large_result));

  constexpr ST_mixed st4{ 0x1842a5 };
  constexpr int i1(3);

  constexpr ST_mixed st5 = st4 << i1;

  ASSERT_EQ(st5.underlying_value(), (st4.underlying_value() << i1));
}

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() >> std::declval<U&>()) != 0,
                        small_result>::type
test_bitwise_right_shift(int);
template<typename T, typename U>
large_result
test_bitwise_right_shift(...);

TEST(test_strong_typedef, bitwise_right_shift)
{
  using ST_plain = jss::strong_typedef<struct Plain, int>;
  using ST_mixed = jss::strong_typedef<
    struct Mixed,
    int,
    jss::strong_typedef_properties::bitwise_right_shift<int>>;

  ASSERT_EQ(sizeof(test_bitwise_right_shift<ST_plain, ST_plain>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_right_shift<ST_plain, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_right_shift<int, ST_plain>(0)),
            sizeof(large_result));

  ASSERT_EQ(sizeof(test_bitwise_right_shift<ST_mixed, ST_mixed>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bitwise_right_shift<ST_mixed, int>(0)),
            sizeof(small_result));
  ASSERT_EQ(sizeof(test_bitwise_right_shift<int, ST_mixed>(0)),
            sizeof(large_result));

  constexpr ST_mixed st4{ 0x1842a5 };
  constexpr int i1(3);

  constexpr ST_mixed st5 = st4 >> i1;

  ASSERT_EQ(st5.underlying_value(), (st4.underlying_value() >> i1));
}

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() += std::declval<U&>()) != 0,
                        small_result>::type
test_plus_equals(int);
template<typename T, typename U>
large_result
test_plus_equals(...);

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() -= std::declval<U&>()) != 0,
                        small_result>::type
test_minus_equals(int);
template<typename T, typename U>
large_result
test_minus_equals(...);

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() *= std::declval<U&>()) != 0,
                        small_result>::type
test_multiply_equals(int);
template<typename T, typename U>
large_result
test_multiply_equals(...);

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() /= std::declval<U&>()) != 0,
                        small_result>::type
test_divide_equals(int);
template<typename T, typename U>
large_result
test_divide_equals(...);

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() <<= std::declval<U&>()) != 0,
                        small_result>::type
test_left_shift_equals(int);
template<typename T, typename U>
large_result
test_left_shift_equals(...);

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() >>= std::declval<U&>()) != 0,
                        small_result>::type
test_right_shift_equals(int);
template<typename T, typename U>
large_result
test_right_shift_equals(...);

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() %= std::declval<U&>()) != 0,
                        small_result>::type
test_mod_equals(int);
template<typename T, typename U>
large_result
test_mod_equals(...);

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() |= std::declval<U&>()) != 0,
                        small_result>::type
test_bit_or_equals(int);
template<typename T, typename U>
large_result
test_bit_or_equals(...);

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() &= std::declval<U&>()) != 0,
                        small_result>::type
test_bit_and_equals(int);
template<typename T, typename U>
large_result
test_bit_and_equals(...);

template<typename T, typename U>
typename std::enable_if<sizeof(std::declval<T&>() ^= std::declval<U&>()) != 0,
                        small_result>::type
test_bit_xor_equals(int);
template<typename T, typename U>
large_result
test_bit_xor_equals(...);

TEST(test_strong_typedef, compound_assignment)
{
  using ST_plain = jss::strong_typedef<struct plain, int>;
  using ST_compound_add =
    jss::strong_typedef<struct compound_add,
                        int,
                        jss::strong_typedef_properties::addable>;
  using ST_compound_sub =
    jss::strong_typedef<struct compound_sub,
                        int,
                        jss::strong_typedef_properties::subtractable>;
  using ST_compound_both =
    jss::strong_typedef<struct compound_sub,
                        int,
                        jss::strong_typedef_properties::subtractable,
                        jss::strong_typedef_properties::addable>;
  using ST_compound_self_mult =
    jss::strong_typedef<struct compound_self_mult,
                        int,
                        jss::strong_typedef_properties::self_multiplicable>;
  using ST_compound_other_mult = jss::strong_typedef<
    struct compound_other_mult,
    int,
    jss::strong_typedef_properties::mixed_multiplicable<int>>;
  using ST_compound_self_divide =
    jss::strong_typedef<struct compound_self_divide,
                        int,
                        jss::strong_typedef_properties::self_divisible>;
  using ST_compound_other_divide =
    jss::strong_typedef<struct compound_other_divide,
                        int,
                        jss::strong_typedef_properties::mixed_divisible<int>>;
  using ST_compound_ratio =
    jss::strong_typedef<struct compound_ratio,
                        int,
                        jss::strong_typedef_properties::ratio<int>>;
  using ST_compound_self_mod =
    jss::strong_typedef<struct compound_self_mod,
                        int,
                        jss::strong_typedef_properties::self_modulus>;
  using ST_compound_other_mod =
    jss::strong_typedef<struct compound_other_mod,
                        int,
                        jss::strong_typedef_properties::mixed_modulus<int>>;
  using ST_compound_self_xor =
    jss::strong_typedef<struct compound_self_xor,
                        int,
                        jss::strong_typedef_properties::self_bitwise_xor>;
  using ST_compound_other_xor =
    jss::strong_typedef<struct compound_other_xor,
                        int,
                        jss::strong_typedef_properties::mixed_bitwise_xor<int>>;
  using ST_compound_self_or =
    jss::strong_typedef<struct compound_self_or,
                        int,
                        jss::strong_typedef_properties::self_bitwise_or>;
  using ST_compound_other_or =
    jss::strong_typedef<struct compound_other_or,
                        int,
                        jss::strong_typedef_properties::mixed_bitwise_or<int>>;
  using ST_compound_self_and =
    jss::strong_typedef<struct compound_self_and,
                        int,
                        jss::strong_typedef_properties::self_bitwise_and>;
  using ST_compound_other_and =
    jss::strong_typedef<struct compound_other_and,
                        int,
                        jss::strong_typedef_properties::mixed_bitwise_and<int>>;
  using ST_compound_left_shift = jss::strong_typedef<
    struct compound_left_shift,
    int,
    jss::strong_typedef_properties::bitwise_left_shift<int>>;
  using ST_compound_right_shift = jss::strong_typedef<
    struct compound_right_shift,
    int,
    jss::strong_typedef_properties::bitwise_right_shift<int>>;

  ASSERT_EQ(sizeof(test_plus_equals<ST_plain, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_plus_equals<ST_compound_add, int>(0)),
            sizeof(small_result));
  ASSERT_EQ(sizeof(test_plus_equals<ST_compound_sub, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_plus_equals<ST_compound_both, int>(0)),
            sizeof(small_result));

  ASSERT_EQ(sizeof(test_plus_equals<ST_plain, ST_plain>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_plus_equals<ST_compound_add, ST_compound_add>(0)),
            sizeof(small_result));
  ASSERT_EQ(sizeof(test_plus_equals<ST_compound_sub, ST_compound_sub>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_plus_equals<ST_compound_both, ST_compound_both>(0)),
            sizeof(small_result));

  ASSERT_EQ(sizeof(test_minus_equals<ST_plain, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_minus_equals<ST_compound_add, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_minus_equals<ST_compound_sub, int>(0)),
            sizeof(small_result));
  ASSERT_EQ(sizeof(test_minus_equals<ST_compound_both, int>(0)),
            sizeof(small_result));

  ASSERT_EQ(sizeof(test_minus_equals<ST_plain, ST_plain>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_minus_equals<ST_compound_add, ST_compound_add>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_minus_equals<ST_compound_sub, ST_compound_sub>(0)),
            sizeof(small_result));
  ASSERT_EQ(sizeof(test_minus_equals<ST_compound_both, ST_compound_both>(0)),
            sizeof(small_result));

  ASSERT_EQ(sizeof(test_multiply_equals<ST_plain, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_divide_equals<ST_plain, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_mod_equals<ST_plain, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_left_shift_equals<ST_plain, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_right_shift_equals<ST_plain, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_or_equals<ST_plain, int>(0)), sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_and_equals<ST_plain, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_xor_equals<ST_plain, int>(0)),
            sizeof(large_result));

  ASSERT_EQ(sizeof(test_multiply_equals<ST_plain, ST_plain>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_divide_equals<ST_plain, ST_plain>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_mod_equals<ST_plain, ST_plain>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_left_shift_equals<ST_plain, ST_plain>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_right_shift_equals<ST_plain, ST_plain>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_or_equals<ST_plain, ST_plain>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_and_equals<ST_plain, ST_plain>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_xor_equals<ST_plain, ST_plain>(0)),
            sizeof(large_result));

  ASSERT_EQ(sizeof(test_multiply_equals<ST_compound_self_mult, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_divide_equals<ST_compound_self_mult, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_mod_equals<ST_compound_self_mult, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_left_shift_equals<ST_compound_self_mult, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_right_shift_equals<ST_compound_self_mult, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_or_equals<ST_compound_self_mult, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_and_equals<ST_compound_self_mult, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_xor_equals<ST_compound_self_mult, int>(0)),
            sizeof(large_result));

  ASSERT_EQ(
    sizeof(
      test_multiply_equals<ST_compound_self_mult, ST_compound_self_mult>(0)),
    sizeof(small_result));
  ASSERT_EQ(
    sizeof(test_divide_equals<ST_compound_self_mult, ST_compound_self_mult>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(test_mod_equals<ST_compound_self_mult, ST_compound_self_mult>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_left_shift_equals<ST_compound_self_mult, ST_compound_self_mult>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_right_shift_equals<ST_compound_self_mult, ST_compound_self_mult>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(test_bit_or_equals<ST_compound_self_mult, ST_compound_self_mult>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_bit_and_equals<ST_compound_self_mult, ST_compound_self_mult>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_bit_xor_equals<ST_compound_self_mult, ST_compound_self_mult>(0)),
    sizeof(large_result));

  ASSERT_EQ(sizeof(test_multiply_equals<ST_compound_other_mult, int>(0)),
            sizeof(small_result));
  ASSERT_EQ(sizeof(test_divide_equals<ST_compound_other_mult, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_mod_equals<ST_compound_other_mult, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_left_shift_equals<ST_compound_other_mult, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_right_shift_equals<ST_compound_other_mult, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_or_equals<ST_compound_other_mult, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_and_equals<ST_compound_other_mult, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_xor_equals<ST_compound_other_mult, int>(0)),
            sizeof(large_result));

  ASSERT_EQ(
    sizeof(
      test_multiply_equals<ST_compound_other_mult, ST_compound_other_mult>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_divide_equals<ST_compound_other_mult, ST_compound_other_mult>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(test_mod_equals<ST_compound_other_mult, ST_compound_other_mult>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_left_shift_equals<ST_compound_other_mult, ST_compound_other_mult>(
        0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_right_shift_equals<ST_compound_other_mult, ST_compound_other_mult>(
        0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_bit_or_equals<ST_compound_other_mult, ST_compound_other_mult>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_bit_and_equals<ST_compound_other_mult, ST_compound_other_mult>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_bit_xor_equals<ST_compound_other_mult, ST_compound_other_mult>(0)),
    sizeof(large_result));

  ASSERT_EQ(sizeof(test_multiply_equals<ST_compound_self_divide, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_divide_equals<ST_compound_self_divide, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_mod_equals<ST_compound_self_divide, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_left_shift_equals<ST_compound_self_divide, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_right_shift_equals<ST_compound_self_divide, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_or_equals<ST_compound_self_divide, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_and_equals<ST_compound_self_divide, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_xor_equals<ST_compound_self_divide, int>(0)),
            sizeof(large_result));

  ASSERT_EQ(
    sizeof(
      test_multiply_equals<ST_compound_self_divide, ST_compound_self_divide>(
        0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_divide_equals<ST_compound_self_divide, ST_compound_self_divide>(0)),
    sizeof(small_result));
  ASSERT_EQ(
    sizeof(
      test_mod_equals<ST_compound_self_divide, ST_compound_self_divide>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_left_shift_equals<ST_compound_self_divide, ST_compound_self_divide>(
        0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_right_shift_equals<ST_compound_self_divide, ST_compound_self_divide>(
        0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_bit_or_equals<ST_compound_self_divide, ST_compound_self_divide>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_bit_and_equals<ST_compound_self_divide, ST_compound_self_divide>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_bit_xor_equals<ST_compound_self_divide, ST_compound_self_divide>(0)),
    sizeof(large_result));

  ASSERT_EQ(sizeof(test_multiply_equals<ST_compound_other_divide, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_divide_equals<ST_compound_other_divide, int>(0)),
            sizeof(small_result));
  ASSERT_EQ(sizeof(test_mod_equals<ST_compound_other_divide, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_left_shift_equals<ST_compound_other_divide, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_right_shift_equals<ST_compound_other_divide, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_or_equals<ST_compound_other_divide, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_and_equals<ST_compound_other_divide, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_xor_equals<ST_compound_other_divide, int>(0)),
            sizeof(large_result));

  ASSERT_EQ(
    sizeof(
      test_multiply_equals<ST_compound_other_divide, ST_compound_other_divide>(
        0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_divide_equals<ST_compound_other_divide, ST_compound_other_divide>(
        0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_mod_equals<ST_compound_other_divide, ST_compound_other_divide>(0)),
    sizeof(large_result));
  ASSERT_EQ(sizeof(test_left_shift_equals<ST_compound_other_divide,
                                          ST_compound_other_divide>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_right_shift_equals<ST_compound_other_divide,
                                           ST_compound_other_divide>(0)),
            sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_bit_or_equals<ST_compound_other_divide, ST_compound_other_divide>(
        0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_bit_and_equals<ST_compound_other_divide, ST_compound_other_divide>(
        0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_bit_xor_equals<ST_compound_other_divide, ST_compound_other_divide>(
        0)),
    sizeof(large_result));

  ASSERT_EQ(sizeof(test_multiply_equals<ST_compound_ratio, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_divide_equals<ST_compound_ratio, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_mod_equals<ST_compound_ratio, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_left_shift_equals<ST_compound_ratio, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_right_shift_equals<ST_compound_ratio, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_or_equals<ST_compound_ratio, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_and_equals<ST_compound_ratio, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_xor_equals<ST_compound_ratio, int>(0)),
            sizeof(large_result));

  ASSERT_EQ(
    sizeof(test_multiply_equals<ST_compound_ratio, ST_compound_ratio>(0)),
    sizeof(large_result));
  ASSERT_EQ(sizeof(test_divide_equals<ST_compound_ratio, ST_compound_ratio>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_mod_equals<ST_compound_ratio, ST_compound_ratio>(0)),
            sizeof(large_result));
  ASSERT_EQ(
    sizeof(test_left_shift_equals<ST_compound_ratio, ST_compound_ratio>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(test_right_shift_equals<ST_compound_ratio, ST_compound_ratio>(0)),
    sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_or_equals<ST_compound_ratio, ST_compound_ratio>(0)),
            sizeof(large_result));
  ASSERT_EQ(
    sizeof(test_bit_and_equals<ST_compound_ratio, ST_compound_ratio>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(test_bit_xor_equals<ST_compound_ratio, ST_compound_ratio>(0)),
    sizeof(large_result));

  ASSERT_EQ(sizeof(test_multiply_equals<ST_compound_self_mod, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_divide_equals<ST_compound_self_mod, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_mod_equals<ST_compound_self_mod, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_left_shift_equals<ST_compound_self_mod, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_right_shift_equals<ST_compound_self_mod, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_or_equals<ST_compound_self_mod, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_and_equals<ST_compound_self_mod, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_xor_equals<ST_compound_self_mod, int>(0)),
            sizeof(large_result));

  ASSERT_EQ(
    sizeof(test_multiply_equals<ST_compound_self_mod, ST_compound_self_mod>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(test_divide_equals<ST_compound_self_mod, ST_compound_self_mod>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(test_mod_equals<ST_compound_self_mod, ST_compound_self_mod>(0)),
    sizeof(small_result));
  ASSERT_EQ(
    sizeof(
      test_left_shift_equals<ST_compound_self_mod, ST_compound_self_mod>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_right_shift_equals<ST_compound_self_mod, ST_compound_self_mod>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(test_bit_or_equals<ST_compound_self_mod, ST_compound_self_mod>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(test_bit_and_equals<ST_compound_self_mod, ST_compound_self_mod>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(test_bit_xor_equals<ST_compound_self_mod, ST_compound_self_mod>(0)),
    sizeof(large_result));

  ASSERT_EQ(sizeof(test_multiply_equals<ST_compound_other_mod, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_divide_equals<ST_compound_other_mod, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_mod_equals<ST_compound_other_mod, int>(0)),
            sizeof(small_result));
  ASSERT_EQ(sizeof(test_left_shift_equals<ST_compound_other_mod, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_right_shift_equals<ST_compound_other_mod, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_or_equals<ST_compound_other_mod, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_and_equals<ST_compound_other_mod, int>(0)),
            sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_xor_equals<ST_compound_other_mod, int>(0)),
            sizeof(large_result));

  ASSERT_EQ(
    sizeof(
      test_multiply_equals<ST_compound_other_mod, ST_compound_other_mod>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(test_divide_equals<ST_compound_other_mod, ST_compound_other_mod>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(test_mod_equals<ST_compound_other_mod, ST_compound_other_mod>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_left_shift_equals<ST_compound_other_mod, ST_compound_other_mod>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_right_shift_equals<ST_compound_other_mod, ST_compound_other_mod>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(test_bit_or_equals<ST_compound_other_mod, ST_compound_other_mod>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_bit_and_equals<ST_compound_other_mod, ST_compound_other_mod>(0)),
    sizeof(large_result));
  ASSERT_EQ(
    sizeof(
      test_bit_xor_equals<ST_compound_other_mod, ST_compound_other_mod>(0)),
    sizeof(large_result));

  ASSERT_EQ(
    sizeof(test_bit_xor_equals<ST_compound_self_xor, ST_compound_self_xor>(0)),
    sizeof(small_result));
  ASSERT_EQ(sizeof(test_bit_xor_equals<ST_compound_self_xor, int>(0)),
            sizeof(large_result));

  ASSERT_EQ(
    sizeof(
      test_bit_xor_equals<ST_compound_other_xor, ST_compound_other_xor>(0)),
    sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_xor_equals<ST_compound_other_xor, int>(0)),
            sizeof(small_result));

  ASSERT_EQ(
    sizeof(test_bit_and_equals<ST_compound_self_and, ST_compound_self_and>(0)),
    sizeof(small_result));
  ASSERT_EQ(sizeof(test_bit_and_equals<ST_compound_self_and, int>(0)),
            sizeof(large_result));

  ASSERT_EQ(
    sizeof(
      test_bit_and_equals<ST_compound_other_and, ST_compound_other_and>(0)),
    sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_and_equals<ST_compound_other_and, int>(0)),
            sizeof(small_result));

  ASSERT_EQ(
    sizeof(test_bit_or_equals<ST_compound_self_or, ST_compound_self_or>(0)),
    sizeof(small_result));
  ASSERT_EQ(sizeof(test_bit_or_equals<ST_compound_self_or, int>(0)),
            sizeof(large_result));

  ASSERT_EQ(
    sizeof(test_bit_or_equals<ST_compound_other_or, ST_compound_other_or>(0)),
    sizeof(large_result));
  ASSERT_EQ(sizeof(test_bit_or_equals<ST_compound_other_or, int>(0)),
            sizeof(small_result));

  ASSERT_EQ(
    sizeof(
      test_left_shift_equals<ST_compound_left_shift, ST_compound_left_shift>(
        0)),
    sizeof(large_result));
  ASSERT_EQ(sizeof(test_left_shift_equals<ST_compound_left_shift, int>(0)),
            sizeof(small_result));

  ASSERT_EQ(
    sizeof(
      test_right_shift_equals<ST_compound_right_shift, ST_compound_right_shift>(
        0)),
    sizeof(large_result));
  ASSERT_EQ(sizeof(test_right_shift_equals<ST_compound_right_shift, int>(0)),
            sizeof(small_result));

  ST_compound_add st1(42);
  st1 += 9;
  EXPECT_EQ(st1.underlying_value(), 51);
  ST_compound_sub st2(42);
  st2 -= 3;
  EXPECT_EQ(st2.underlying_value(), 39);
  ST_compound_self_mult st3(6);
  ST_compound_self_mult st4(7);
  st3 *= st4;
  EXPECT_EQ(st3.underlying_value(), 42);

  ST_compound_other_mult st5(9);
  st5 *= 6;
  EXPECT_EQ(st5.underlying_value(), 54);
}

TEST(test_strong_typedef, adding_two_strong_typedefs)
{
  using ST1 = jss::strong_typedef<struct tag1, int>;
  using ST2 =
    jss::strong_typedef<struct tag2,
                        int,
                        jss::strong_typedef_properties::mixed_addable<ST1>>;

  constexpr ST1 st1(23);
  constexpr ST2 st2(46);

  constexpr ST2 st3 = st2 + st1;
  constexpr ST2 st4 = st1 + st2;

  ASSERT_EQ(st3.underlying_value(), 69);
  ASSERT_EQ(st4.underlying_value(), 69);

  using STS1 = jss::strong_typedef<struct tag1, std::string>;
  using STS2 =
    jss::strong_typedef<struct tag2,
                        std::string,
                        jss::strong_typedef_properties::mixed_addable<STS1>>;

  STS1 sts1("hello");
  STS2 sts2("world");

  STS2 sts3 = sts2 + sts1;
  STS2 sts4 = sts1 + sts2;

  EXPECT_EQ(sts3.underlying_value(), "worldhello");
  EXPECT_EQ(sts4.underlying_value(), "helloworld");
}
